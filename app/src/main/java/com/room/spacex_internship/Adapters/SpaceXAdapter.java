package com.room.spacex_internship.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.room.spacex_internship.Database.RoomDB;
import com.room.spacex_internship.R;
import com.room.spacex_internship.Database.SpaceXData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SpaceXAdapter extends RecyclerView.Adapter<SpaceXAdapter.ViewHolder> {
    
    ArrayList<SpaceXData> modelList;
    Context context;
    
    RoomDB roomDB;
    Target target;
    Bitmap bitmap;
    
    public SpaceXAdapter(ArrayList<SpaceXData> modelList, RoomDB roomDB, Context context) {
        this.modelList = modelList;
        this.context = context;
        this.roomDB = roomDB;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spacex_crew, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return modelList.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        
        SpaceXData data = modelList.get(position);
        Log.e("IMAGE", data.getImage());
    
        Picasso.get()
                .load(data.getImage())
                .resize(200, 200)
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                    
                    }
    
                    @Override
                    public void onError(Exception e) {
                        //Try again online if cache failed
                        Picasso.get()
                                .load(data.getImage())
                                .placeholder(R.drawable.image_loading_placeholder)
                                .resize(200, 200)
                                .centerCrop()
                                .into(holder.image, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    
                                    }
                                
                                    @Override
                                    public void onError(Exception e) {
                                        Log.v("Picasso","Could not fetch image " + e.toString());
                                    }
                                });
                    }
                });
        
        holder.name.setText(data.getName());
        
        holder.agency.setText("Agency: " + data.getAgency());
        
        holder.wikipedia.setClickable(true);
        holder.wikipedia.setMovementMethod(LinkMovementMethod.getInstance());
        holder.wikipedia.setText(Html.fromHtml("<a href = '" + data.getWikipedia() + "' >" + data.getWikipedia() + "</a>"));
    
        holder.status.setText(capitalizeFirstLetter(data.getStatus()));
        if(data.getStatus().equalsIgnoreCase("active")){
            holder.status.setTextColor(Color.GREEN);
        }
        else{
            holder.status.setTextColor(Color.RED);
        }
        
    }
    
    String capitalizeFirstLetter(String str){
        String letter = (str.charAt(0) + "").toUpperCase();
        str = letter + str.substring(1);
        return str;
    }
    
    private String createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {
        
        File direct = new File(Environment.getExternalStorageDirectory() + "/SpaceX");
        
        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/SpaceX/");
            wallpaperDirectory.mkdirs();
        }
        
        File file = new File("/sdcard/SpaceX/", fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return file.getAbsolutePath();
    }
    
    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        TextView name, agency, wikipedia, status;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            agency = itemView.findViewById(R.id.agency);
            wikipedia = itemView.findViewById(R.id.wikipedia);
            status = itemView.findViewById(R.id.status);
        }
    }
}
