package com.room.spacex_internship.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.room.spacex_internship.Misc.Constants;

@Database(entities = {SpaceXData.class}, version = 1, exportSchema = false)
public abstract class RoomDB extends RoomDatabase {
    private static RoomDB roomDB;
    private static String DATABASE_NAME = Constants.DB_NAME;
    
    public synchronized static RoomDB getInstance(Context context) {
        if (roomDB == null) {
            roomDB = Room.databaseBuilder(context,
                    RoomDB.class,
                    DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return roomDB;
    }
    
    public abstract SpaceX_DAO mainDAO();
}
