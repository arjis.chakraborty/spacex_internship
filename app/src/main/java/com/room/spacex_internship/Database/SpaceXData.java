package com.room.spacex_internship.Database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "crew")
public class SpaceXData implements Serializable {
    
    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String id;
    
    @ColumnInfo(name = "name", defaultValue = "")
    private String name;
    
    @ColumnInfo(name = "agency", defaultValue = "")
    private String agency;
    
    @ColumnInfo(name = "image", defaultValue = "")
    private String image;
    
    @ColumnInfo(name = "wikipedia", defaultValue = "")
    private String wikipedia;
    
    @ColumnInfo(name = "status", defaultValue = "")
    private String status;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getAgency() {
        return agency;
    }
    
    public void setAgency(String agency) {
        this.agency = agency;
    }
    
    public String getImage() {
        return image;
    }
    
    public void setImage(String image) {
        this.image = image;
    }
    
    public String getWikipedia() {
        return wikipedia;
    }
    
    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
}
