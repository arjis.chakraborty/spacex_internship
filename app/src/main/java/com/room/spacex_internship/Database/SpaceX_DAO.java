package com.room.spacex_internship.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SpaceX_DAO {
    
    @Insert(onConflict = REPLACE)
    void insert(SpaceXData data);
    
    @Delete
    void delete(ArrayList<SpaceXData> data);
    
    @Query("SELECT * FROM crew")
    List<SpaceXData> getAll();
    
    @Query("UPDATE crew SET image = :image WHERE id = :id")
    void update(String image, String id);
}
