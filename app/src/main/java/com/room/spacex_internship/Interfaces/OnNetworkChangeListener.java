package com.room.spacex_internship.Interfaces;

public interface OnNetworkChangeListener {
    void onNetworkChanged(boolean isNetworkAvailable);
}
