package com.room.spacex_internship.Interfaces;

public interface OnResponseListener {
    void onResponseReceived(String response);
}
