package com.room.spacex_internship;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.room.spacex_internship.Adapters.SpaceXAdapter;
import com.room.spacex_internship.Interfaces.OnNetworkChangeListener;
import com.room.spacex_internship.Interfaces.OnResponseListener;
import com.room.spacex_internship.Misc.Constants;
import com.room.spacex_internship.Receivers.NetworkReceiver;
import com.room.spacex_internship.Database.RoomDB;
import com.room.spacex_internship.Database.SpaceXData;
import com.room.spacex_internship.VolleyUtils.APIEndpoints;
import com.room.spacex_internship.VolleyUtils.RequestAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnNetworkChangeListener, View.OnClickListener {
    
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    
    @BindView(R.id.loadingLayout)
    RelativeLayout loadingLayout;
    
    @BindView(R.id.delete)
    FloatingActionButton delete;
    
    @BindView(R.id.refresh)
    FloatingActionButton refresh;
    
    @BindView(R.id.fabMenu)
    FloatingActionMenu fabMenu;
    
    @BindView(R.id.noDataLayout)
    RelativeLayout noDataLayout;
    
    SpaceXAdapter adapter;
    NetworkReceiver networkReceiver;
    boolean isNetworkAvailable;
    boolean dataLoaded = false;
    
    RoomDB roomDB;
    
    ArrayList<SpaceXData> modelList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        
        delete.setOnClickListener(this);
        refresh.setOnClickListener(this);
        
        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        checkPerms(perms, Constants.PERMS_REQUEST_CODE);
        
        networkReceiver = new NetworkReceiver(this);
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
        
        roomDB = RoomDB.getInstance(this);
        
        modelList = new ArrayList<>();
        
        /*if (isNetworkAvailable) {
            callAPI();
            Toast.makeText(this, "Syncing...", Toast.LENGTH_SHORT).show();
        } else {
            //show data from room
            //Toast.makeText(this, "Network unavailable. Data may not be upto date.", Toast.LENGTH_SHORT).show();
            parseDataFromDB();
        }*/
    }
    
    void checkPerms(String[] perms, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(MainActivity.this, perms, requestCode);
        }
        
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkReceiver);
    }
    
    void callAPI() {
        loadingLayout.setVisibility(View.VISIBLE);
        modelList.clear();
        roomDB.mainDAO().delete(modelList);
        RequestAPI requestAPI = new RequestAPI(APIEndpoints.CREW_ENDPOINT, this);
        requestAPI.get(new OnResponseListener() {
            @Override
            public void onResponseReceived(String response) {
                try {
                    JSONArray body = new JSONArray(response);
                    for (int i = 0; i < body.length(); i++) {
                        JSONObject obj = body.optJSONObject(i);
                        SpaceXData model = new SpaceXData();
                        model.setId(obj.optString("id"));
                        model.setAgency(obj.optString("agency"));
                        model.setWikipedia(obj.optString("wikipedia"));
                        model.setImage(obj.optString("image"));
                        model.setName(obj.optString("name"));
                        model.setStatus(obj.optString("status"));
                        
                        modelList.add(model);
                        roomDB.mainDAO().insert(model);
                    }
                    adapter = new SpaceXAdapter(modelList, roomDB, MainActivity.this);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    recyclerView.setAdapter(adapter);
                    
                    loadingLayout.setVisibility(View.GONE);
                    if (modelList.size() == 0) {
                        noDataLayout.setVisibility(View.VISIBLE);
                    }
                    else{
                        noDataLayout.setVisibility(View.GONE);
                    }
                    dataLoaded = true;
                } catch (JSONException e) {
                    Log.e("Error", e.toString());
                }
            }
        });
        
    }
    
    void parseDataFromDB() {
        loadingLayout.setVisibility(View.GONE);
        modelList.clear();
        modelList.addAll(roomDB.mainDAO().getAll());
        adapter = new SpaceXAdapter(modelList, roomDB, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
    
    @Override
    public void onNetworkChanged(boolean isNetworkAvailable) {
        this.isNetworkAvailable = isNetworkAvailable;
        if (isNetworkAvailable) {
            modelList.clear();
            callAPI();
            Toast.makeText(this, "Syncing...", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Network disconnected. Data may not be upto date.", Toast.LENGTH_SHORT).show();
            if (!dataLoaded)
                parseDataFromDB();
        }
    
        if (modelList.size() == 0) {
            noDataLayout.setVisibility(View.VISIBLE);
        }
        else{
            noDataLayout.setVisibility(View.GONE);
        }
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.refresh:
                refreshData();
                fabMenu.close(true);
                break;
            
            case R.id.delete:
                deleteData();
                fabMenu.close(true);
                break;
        }
    }
    
    void refreshData() {
        if (isNetworkAvailable) {
            Toast.makeText(this, "Refreshing...", Toast.LENGTH_SHORT).show();
            callAPI();
        } else {
            Toast.makeText(this, "Network unavailable! Connect to a network and try again.", Toast.LENGTH_SHORT).show();
        }
    }
    
    void deleteData() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Delete all data?")
                .setMessage("This will delete all your saved data. Are you sure?")
                .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        roomDB.mainDAO().delete(modelList);
                        Toast.makeText(MainActivity.this, "Successfully deleted all data!", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    
                    }
                })
                .create();
        
        dialog.show();
    }
}