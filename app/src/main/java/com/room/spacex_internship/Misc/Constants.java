package com.room.spacex_internship.Misc;

public class Constants {
    
    public static String DB_NAME = "space_x_db";
    
    public static boolean IS_NETWORK_AVAILABLE = false;
    public static int PERMS_REQUEST_CODE = 10011;
}
