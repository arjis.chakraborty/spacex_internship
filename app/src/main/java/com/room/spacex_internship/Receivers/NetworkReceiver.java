package com.room.spacex_internship.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.room.spacex_internship.Interfaces.OnNetworkChangeListener;
import com.room.spacex_internship.Misc.Constants;

public class NetworkReceiver extends BroadcastReceiver {
    OnNetworkChangeListener listener;
    
    public NetworkReceiver(OnNetworkChangeListener listener) {
        this.listener = listener;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                Log.e("Network", "Internet YAY");
                listener.onNetworkChanged(true);
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                Log.e("Network", "No internet :(");
                listener.onNetworkChanged(false);
            }
        }
    }
}
