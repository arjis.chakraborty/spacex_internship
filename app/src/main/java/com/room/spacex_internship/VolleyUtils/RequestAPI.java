package com.room.spacex_internship.VolleyUtils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.room.spacex_internship.Interfaces.OnResponseListener;
import com.room.spacex_internship.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestAPI {
    String endpoint;
    Context context;
    
    public RequestAPI(String endpoint, Context context) {
        this.endpoint = endpoint;
        this.context = context;
    }
    
    public void get(OnResponseListener listener) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, endpoint, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                listener.onResponseReceived(response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("Error", error.toString());
            }
        });
        
        requestQueue.add(req);
    }
}
